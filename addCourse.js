let courseNameInput = document.querySelector("#course-name-input")
let courseDescriptionInput = document.querySelector("#course-description-input")
let coursePriceInput = document.querySelector("#course-price-input")
let token = localStorage.getItem('token')

document.querySelector('#form-addCourse').addEventListener('submit',(e)=>{
	e.preventDefault()

	console.log(token)
	console.log(courseNameInput.value)
	console.log(courseDescriptionInput.value)
	console.log(coursePriceInput.value)
	fetch('http://localhost:4000/courses',{

		method: 'POST',
		headers:{
			'Authorization': `Bearer ${token}`,
			'Content-Type':'application/json'
		},
		body: JSON.stringify({
			name:courseNameInput.value,
			description:courseDescriptionInput.value,
			price:coursePriceInput.value

		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)
	})
})