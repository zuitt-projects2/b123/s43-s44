let emailInput = document.querySelector("#email-input")
let passwordInput = document.querySelector("#password-input")

document.querySelector('#form-login').addEventListener('submit',(e)=>{

	e.preventDefault()
	fetch('http://localhost:4000/users/login',{

		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			email:emailInput.value,
			password:passwordInput.value
		})
	})
	.then(res => res.json())
	.then(data => {
		//console.log(data)
		//localStorage - is an object in JS which will allow us to save small amount of data within browsers. We can use this to save our token. localStorage exists in most modern browsers.
		//localStorage.setItem() will allow us to save data in our browsers. However, any data that we pass to our localStorage will become a string.
		//syntax: localStorage.setItem(<key>,<value>)
		localStorage.setItem('token',data.accessToken)
	})

})