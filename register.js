let firstNameInput = document.querySelector("#first-name-input")
let lastNameInput = document.querySelector("#last-name-input")
let mobileNoInput = document.querySelector("#mobile-num-input")
let emailInput = document.querySelector("#email-input")
let passwordInput = document.querySelector("#password-input")

document.querySelector('#form-register').addEventListener('submit',(e)=>{

	//event object - contains details about our event and is received by any function added in an addEventListener. This event object contains details about the exact event, details about which element triggered the event, it even has details about the values of the element that triggered the event

	//submit will, by default, refresh your page instead
	e.preventDefault()

	console.log(firstNameInput.value)
	console.log(lastNameInput.value)
	console.log(mobileNoInput.value)
	console.log(emailInput.value)
	console.log(passwordInput.value)

	//can we send these values to our api?
	//GET method request only would need the URL or headers for tokens
	//however other request with a different HTTP method, we would have to explicitly declare:
	//options is object which will contain:
		//headers - authorization tokens or content type headers
		//HTTP method
		//body of our request
	//syntax fetch(<request>,{options})

	fetch('http://localhost:4000/users/',{

		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			firstName: firstNameInput.value,
			lastName: lastNameInput.value,
			email: emailInput.value,
			mobileNo: mobileNoInput.value,
			password: passwordInput.value
		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)
	})
})
