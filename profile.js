//get user details

//syntax: localStorage.getItem(<key>) - This will return the value of the key from our localStorage
let token = localStorage.getItem('token')
/*console.log(token)*/
let profName = document.querySelector("#profile-name")
let profEmail = document.querySelector("#profile-email")
let profMobile = document.querySelector("#profile-mobile")

fetch('http://localhost:4000/users/getUserDetails',{

	headers:{
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	console.log(data)

//straight document.querySelector("#profile-mobile").innerHTML = `Mobile: ${data.mobileNo}`

	profName.innerHTML = `Hello, ${data.firstName} ${data.lastName}`
	profEmail.innerHTML = `Email: ${data.email}`
	profMobile.innerHTML = `Mobile: ${data.mobileNo}`

	
})